BUD-872

METHOD: POST

URL: http://localhost:49419/Services/QuoteHome/QuoteService.svc/IOSDeleteQuote

Request:
{
	"baseEntity": {
		"BaseDBName": "BuddyCRMDB_001",
		"BaseSubscriptionID": "Buddy_001",
		"CustomEditId": 0,
		"BaseUserID": 1
	},
	"quoteID":20070
}

Response:
{
  "IOSDeleteQuoteResult": {
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "Message": "Quote deleted successfully",
    "ScalerData": null
  }
}

