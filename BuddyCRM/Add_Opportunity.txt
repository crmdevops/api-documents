BUD-765

API NAME: Save Opportunities from dynamic form API

METHOD:POST

URL:http://localhost:49419/Services/Common/OpportunityService.svc/IOSAddEditOpportunity

Request:{
	"CustomerEntity": {
		"OpportunityList": [{
			"ID": "38",
			"FieldName": "OpportunityName",
			"Value": "FidlerOpportunity1",
			"FieldNameMaster": "OpportunityName",
			"IsExisting": 1

		}, {
			"ID": "39",
			"FieldName": "OpportunityValue",
			"Value": "30",
			"FieldNameMaster": "OpportunityValue",
			"IsExisting": 1

		}, {
			"ID": "40",
			"FieldName": "OpportunityStatusID",
			"Value": "1",
			"FieldNameMaster": "OpportunityStatusID",
			"IsExisting": 1
		}, {
			"ID": "41",
			"FieldName": "ClosingDate",
			"Value": "04/06/2017",
			"FieldNameMaster": "ClosingDate",
			"IsExisting": 1
		}, 
{
			"ID": "6",
			"FieldName": "OwnerID",
			"Value": "1",
			"FieldNameMaster": "OwnerID",
			"IsExisting": 1
		},
{
			"ID": "42",
			"FieldName": "Probability",
			"Value": "30",
			"FieldNameMaster": "Probability",
			"IsExisting": 1,
			"FieldDataType": "CheckBoxList",
			"dropDownValues": [{
				"ID": "1",
				"IsSelected": 1
			}, {
				"ID": "2",
				"IsSelected": 1
			}]
		}, {
			"ID": "3256",
			"FieldName": "Dropdown",
			"Value": "3",
			"FieldNameMaster": "Dropdown",
			"IsExisting": 0
		}]
	},
	"baseEntity": {
		"BaseUserID": 1,
		"BaseSubscriptionID": "Buddy_001",
		"BaseDBName": "BuddyCRMDB_001",
		"CustomEditId": 0
	},
"AccountId":"107123"

}



Response:
{
  "IOSAddEditOpportunityResult": {
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "Message": "SUCCESS",
    "ScalerData": null
  }
}
