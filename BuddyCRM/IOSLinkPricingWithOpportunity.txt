BUD-1229

METHOD:-POST

METHOD NAME:- TO SAVE PRODUCT

URL:-http://localhost:49419/services/Common/GenericService.svc/IOSLinkPricingWithOpportunity


REQUEST:-
{
	
	"baseEntity" : {
      "BaseDBName" : "BuddyCRMDB_001",
      "BaseSubscriptionID" : "Buddy_001",
      "CustomEditId" : 0,
      "BaseUserID" : 1    
  },
  "opportunityID":96167,
  "productID":8
  
}

RESPONSE:-


{
  "IOSLinkPricingWithOpportunityResult": {
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "SubscriptionType": null,
    "Code": 103,
    "CustomColumns": null,
    "Data": null,
    "Message": "Something Went Wrong Please Try Again Later",
    "ScalerData": null
  }
}

