BUD-767

API NAME:Update Opportunities from dynamic form API

METHOD :POST

URL:http://localhost:49419/services/Common/opportunityservice.svc/IOSAddEditOpportunity

REQUEST:{
	"CustomerEntity": {
		"OpportunityList": [{
			"ID": "38",
			"FieldName": "OpportunityName",
			"Value": "sandeep tests",
			"FieldNameMaster": "OpportunityName",
			"IsExisting": 1

		}, {
			"ID": "39",
			"FieldName": "OpportunityValue",
			"Value": "123134",
			"FieldNameMaster": "OpportunityValue",
			"IsExisting": 1

		}, {
			"ID": "40",
			"FieldName": "OpportunityStatusID",
			"Value": "1",
			"FieldNameMaster": "OpportunityStatusID",
			"IsExisting": 1
		}, {
			"ID": "41",
			"FieldName": "ClosingDate",
			"Value": "03/09/2017",
			"FieldNameMaster": "ClosingDate",
			"IsExisting": 1
		}, {
			"ID": "6",
			"FieldName": "OwnerID",
			"Value": "1",
			"FieldNameMaster": "OwnerID",
			"IsExisting": 1
		}, {
			"ID": "42",
			"FieldName": "Probability",
			"Value": "30",
			"FieldNameMaster": "Probability",
			"IsExisting": 1,
			"FieldDataType": "CheckBoxList",
			"dropDownValues": [{
				"ID": "1",
				"IsSelected": 1
			}, {
				"ID": "2",
				"IsSelected": 1
			}]
		}, {
			"ID": "3256",
			"FieldName": "Dropdown",
			"Value": "11",
			"FieldNameMaster": "Dropdown",
			"IsExisting": 0
		}]
	},
	"baseEntity": {
		"BaseUserID": 1,
		"BaseSubscriptionID": "Buddy_001",
		"BaseDBName": "BuddyCRMDB_001",
		"CustomEditId": 0
	},
	"AccountId": "107123",
             "OpportunityId":"96159"

}


Response:
{
  "IOSAddEditOpportunityResult": {
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "Message": "SUCCESS",
    "ScalerData": null
  }
}