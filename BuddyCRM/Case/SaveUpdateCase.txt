Request URL: 

http://35.176.25.71:8082/Services/Case/CasesService.svc/SaveUpdateCase

Request header:

Content-Type: application/json
Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiUXJRRHZoUHRQcWFROG13TG92MVJvcmNwSjBrcXYvdytzMytrNEk2OWJXK2tiMEhVcHYzV1dBPT0iLCJpYXQiOiIxNjMxNjAzMDc1IiwiVXNlcklkIjoiNTR6RE93UTBvYTg9IiwiRm9yIjoiQUIwaUZjY3g3VHlrdmZzK0dnWWMyYWNvcnZHQ2FFQmwycG84OG82K3NBdz0ifQ.nvUqPb7yLA_Nfq3guVbvYzG4J_x_qr3_WdAQiqlU4Zk

{
    "TemplateID": "0",
    "CaseID": 0,
    "Subject": "test case",
    "CompanyID": "36532",
    "SelectedContactIDsString": "21187",
    "CaseStatusID": "0",
    "CaseTypeID": "0",
    "CasePriorityID": "0",
    "AssignedTo": 0,
    "SelectedAssignedCCIDsString": "",
    "LinkedProductIds": "",
    "Description": "",
    "baseEntity": {
        "BaseSubscriptionID": "Buddy_002",
        "BaseDBName": "uWCW0JNq7u+vXl5WuVUCEA==",
        "BaseUserName": "Philip Mayling",
        "SubscriptionType": "company",
        "UserBaseID": "54zDOwQ0oa8=",
        "CustomEditId": 0
    },
    "PendingActionID": 0,
    "LinkedActivity": null,
    "CustomColumnsData": [
        {
            "Value": "",
            "FieldId": 643,
            "FeildDataType": "Text"
        },
        {
            "Value": "",
            "FieldId": 652,
            "FeildDataType": "Date"
        },
        {
            "Value": "",
            "FieldId": 644,
            "FeildDataType": "Numeric"
        },
        {
            "Value": "",
            "FieldId": 648,
            "FeildDataType": "TextArea"
        },
        {
            "Value": false,
            "FieldId": 650,
            "FeildDataType": "checkbox"
        },
        {
            "Value": "",
            "FieldId": 651,
            "FeildDataType": "Dropdown"
        }
    ]
}

Response:

{
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "BaseUserName": null,
    "CustomEditId": 0,
    "DataTypevalue": null,
    "Domain": null,
    "FieldName": null,
    "Ip": null,
    "IsForNewUI": 0,
    "ObjID": 0,
    "SearchItem": null,
    "SearchProductForProductPricing": null,
    "SubscriptionType": null,
    "UserBaseID": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "IsPending": 0,
    "Message": "SUCCESS",
    "MinNextReminderTime": null,
    "ScalerData": {
        "ID": 10434,
        "IsConvertAcc": 0,
        "Message": null,
        "isPending": 0
    },
    "SuppInfoArr": null,
    "TotalPage": 0,
    "TotalRecord": 0,
    "TotalRecords": 0,
    "UpdateCode": 0
}