Method:POST

URL:http://localhost:49419/services/CustomerHome/pricingService.svc/IOSDeleteProductPricing

Request:

{

	"ProductId": "11",	
	"baseEntity": {
		"BaseUserID": 1,
		"BaseSubscriptionID": "Buddy_001",
		"BaseDBName": "BuddyCRMDB_001",
		"BaseUserName": "Philip Mayling",
		"CustomEditId": 0
	}

}

Response:
{
  "IOSDeleteProductPricingResult": {
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "Message": "Product deleted successfully.",
    "ScalerData": null
  }
}