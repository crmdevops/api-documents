

API NAME :- Get Sales order product details for sage

URL:-http://localhost:49419/Services/Common/GenericService.svc/IOSGetSalesOrderProduct

Request:-
{
  
    "accountID": "31661",
    "orderNumber":"14",
    "baseEntity": {
      "BaseUserID": 4,
      "BaseSubscriptionID": "Buddy_50",
      "BaseDBName": "BuddyCRMDB_50",
      "BaseUserName": "Philip Mayling",
      "CustomEditId": 0
    }
}
  


Response:-
{
    "IOSGetSalesOrderProductResult": {
        "BaseDBName": null,
        "BaseSubscriptionID": null,
        "BaseUserID": 0,
        "CustomEditId": 0,
        "FieldName": null,
        "Ip": null,
        "ObjID": 0,
        "SubscriptionType": null,
        "Code": 100,
        "CustomColumns": null,
        "Data": [
            {
                "Comment1": "UPVC 8mm Grey",
                "Comment2": "",
                "Description": "UPVC 8mm Grey",
                "NetAmount": 210.75,
                "ProductCode": "U8GREY",
                "Qty_Order": 1,
                "Unit_Price": 0
            }
        ],
        "IsPending": 0,
        "Message": "SUCCESS",
        "ScalerData": null
    }
}