BUD-2346

API NAME:- Generate Quote PDF

URL:-http://localhost:49419//Services/QuoteHome/QuoteService.svc/IOSGenerateQuotePDF

Request:-
{
  "baseEntity" : {
    "SubscriptionType" : "contact",
    "BaseDBName" : "BuddyCRMDB_50",
    "BaseSubscriptionID" : "Buddy_50",
    "CustomEditId" : 0,
    "BaseUserID" : 15
  },
  "quoteID" : 48
}


Response:-
{
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "ObjID": 0,
    "SubscriptionType": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "IsPending": 0,
    "Message": "SUCCESS",
    "ScalerData": "http://192.168.12.20:8288/DownloadedQuotePDF\\11-10-2017_48.pdf"
}




