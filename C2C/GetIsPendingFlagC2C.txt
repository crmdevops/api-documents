
BUD-1913

API NAME:- Get IsPending Flag for C2C

URL:-http://localhost:49419/Services/Common/GenericService.svc/IOSCntGetIsPendingFlag

Request:-
{
  "baseEntity" : {
    "SubscriptionType" : "contact",
    "BaseDBName" : "BuddyCRMDB_50",
    "BaseSubscriptionID" : "Buddy_50",
    "CustomEditId" : 0,
    "BaseUserID" : 15
  }

}

Response:-
{
    "IOSCntGetIsPendingFlagResult": {
        "BaseDBName": "BuddyCRMDB_50",
        "BaseSubscriptionID": "Buddy_50",
        "BaseUserID": 15,
        "CustomEditId": 0,
        "FieldName": null,
        "Ip": null,
        "SubscriptionType": "contact",
        "Code": 100,
        "CustomColumns": null,
        "Data": null,
        "IsPending": 0,
        "Message": "SUCCESS",
        "ScalerData": null
    }
}
