BUD-1225

API NAME:- Get Lead/Account details by AccountID C2C


URL:-http://localhost:49419/Services/Common/CustomerService.svc/IosCnt_GetRecordDetailsByAccountID

Request:
{
	"customer": {
		"AccountID": "20410",
		"BaseEntityObj": {
			"BaseUserID": 1,
			"BaseSubscriptionID": "Buddy_50",
			"BaseDBName": "BuddyCRMDB_50",
			"BaseUserName": "Philip Mayling",
			"CustomEditId": 0
		}
	},
	"IsDropDownvaluesRequired": "0"
}


Response:-
{
  "BaseDBName": null,
  "BaseSubscriptionID": null,
  "BaseUserID": 0,
  "CustomEditId": 0,
  "FieldName": null,
  "Ip": null,
  "SubscriptionType": null,
  "Code": 100,
  "CustomColumns": null,
  "Data": null,
  "Message": "SUCCESS",
  "ScalerData": {
    "AdvancedContactList": null,
    "BasicContactList": null,
    "EmailClientList": null,
    "OpportunityList": null,
    "RecordId": 0,
    "masterFields": [
      {
        "FieldDataType": "checkbox",
        "FieldName": "Post optout",
        "FieldNameMaster": "Post optout",
        "ID": 1290,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "0",
        "Value": "0",
        "dropDownValues": null
      },
      {
        "FieldDataType": "checkbox",
        "FieldName": "Telephone optout",
        "FieldNameMaster": "Telephone optout",
        "ID": 1291,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "0",
        "Value": "0",
        "dropDownValues": null
      },
      {
        "FieldDataType": "checkbox",
        "FieldName": "Email Promos optout",
        "FieldNameMaster": "Email Promos optout",
        "ID": 1292,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "0",
        "Value": "0",
        "dropDownValues": null
      },
      {
        "FieldDataType": "checkbox",
        "FieldName": "Email Newsletters optout",
        "FieldNameMaster": "Email Newsletters optout",
        "ID": 1293,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "0",
        "Value": "0",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "FirstName",
        "FieldNameMaster": "FirstName",
        "ID": 15,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "2",
        "Value": "Rahul",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "LastName",
        "FieldNameMaster": "LastName",
        "ID": 16,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "3",
        "Value": "Bose",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "ZipCode",
        "FieldNameMaster": "ZipCode",
        "ID": 36,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "4",
        "Value": "121001",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Email",
        "FieldNameMaster": "Email",
        "ID": 18,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "5",
        "Value": "Developer53@gmail.com",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Owner",
        "FieldNameMaster": "OwnerID",
        "ID": 6,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "5",
        "Value": "Ritam Gandhi",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Twitter",
        "FieldNameMaster": "Twitter",
        "ID": 9,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "6",
        "Value": "http://www.fidlerTwitter.com",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Facebook",
        "FieldNameMaster": "Facebook",
        "ID": 10,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "7",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Building",
        "FieldNameMaster": "Building",
        "ID": 11,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "8",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Street",
        "FieldNameMaster": "Street",
        "ID": 12,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "9",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "City",
        "FieldNameMaster": "City",
        "ID": 13,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "10",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "State",
        "FieldNameMaster": "State",
        "ID": 34,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "11",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "DropDown",
        "FieldName": "Country",
        "FieldNameMaster": "CountryID",
        "ID": 35,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "12",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Phone",
        "FieldNameMaster": "Phone",
        "ID": 37,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "14",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Numeric",
        "FieldName": "Mobile",
        "FieldNameMaster": "Mobile",
        "ID": 1281,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "17",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Text",
        "FieldName": "Master Text",
        "FieldNameMaster": "Master Text",
        "ID": 1436,
        "IsExisting": false,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "20",
        "Value": null,
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Master dropdown",
        "FieldNameMaster": "Master dropdown",
        "ID": 1437,
        "IsExisting": false,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "21",
        "Value": null,
        "dropDownValues": null
      },
      {
        "FieldDataType": "Date",
        "FieldName": "Master date",
        "FieldNameMaster": "Master date",
        "ID": 1438,
        "IsExisting": false,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "22",
        "Value": null,
        "dropDownValues": null
      }
    ],
    "tradingFields": [
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Account Profile",
        "FieldNameMaster": "AccountProfile",
        "ID": 19,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "1",
        "Value": "5000-10000 GBP",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Lead Status",
        "FieldNameMaster": "LeadStatus",
        "ID": 1269,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "2",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "LeadSource",
        "FieldNameMaster": "LeadSource",
        "ID": 21,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "3",
        "Value": "E-Mail Campaign",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Contact Frequency",
        "FieldNameMaster": "ContactFrequency",
        "ID": 22,
        "IsExisting": true,
        "IsMandatory": true,
        "IsVisible": true,
        "Priority": "4",
        "Value": "BiMonthly",
        "dropDownValues": null
      },
      {
        "FieldDataType": "DropDown",
        "FieldName": "BudgetYear StartDate",
        "FieldNameMaster": "BudgetYearStartDate",
        "ID": 24,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "6",
        "Value": "Feb",
        "dropDownValues": null
      },
      {
        "FieldDataType": "DropDown",
        "FieldName": "Usual SpendDate",
        "FieldNameMaster": "UsualSpendDate",
        "ID": 25,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "7",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Campaign",
        "FieldNameMaster": "CampaignID",
        "ID": 1088,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "7",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Industry",
        "FieldNameMaster": "Industry",
        "ID": 28,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "10",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Numeric",
        "FieldName": "Annual Revenue",
        "FieldNameMaster": "AnnualRevenue",
        "ID": 29,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "11",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Numeric",
        "FieldName": "Credit Terms",
        "FieldNameMaster": "CreditTerms",
        "ID": 32,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "14",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Numeric",
        "FieldName": "Credit Limit",
        "FieldNameMaster": "CreditLimit",
        "ID": 33,
        "IsExisting": true,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "15",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Date",
        "FieldName": "trading date",
        "FieldNameMaster": "trading date",
        "ID": 1439,
        "IsExisting": false,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "16",
        "Value": "",
        "dropDownValues": null
      },
      {
        "FieldDataType": "Dropdown",
        "FieldName": "Trading drop",
        "FieldNameMaster": "Trading drop",
        "ID": 1440,
        "IsExisting": false,
        "IsMandatory": false,
        "IsVisible": true,
        "Priority": "17",
        "Value": "",
        "dropDownValues": null
      }
    ]
  }
}
