BUD-1847

API NAME:- Update Quotes For C2C

URL:-http://localhost:49419/Services/QuoteHome/QuoteService.svc/IOSCntUpdateQuote

Request:-
{
	"baseEntity": {
		"BaseDBName": "BuddyCRMDB_50",
		"BaseSubscriptionID": "Buddy_50",
		"CustomEditId": 0,
		"BaseUserID": 15
	},
	"iosQuoteEntity": {
	"AccountID": 21298,
		"AddedUserId": 1,
		"CompanyName": "",
		"ContactID": 0,
		"ContactType": "Lead",
		"CreateDate": "",
		"IsDeleted": "false",
		"IsDrafted": "false",
		"LastUpdateDate": "",
		"LastViewDate": "",
		"NoLines": 0,
		"OpportunityID": 0,
		"OwnerID": 0,
		"OwnerName": "",
		"QID": 10234,
		"QuoteID": 0,
		"QuoteName": "Test Quote abcd 1234",
		"QuoteNote": "",
		"QuoteNum": 0,
		"QuoteRecordAddress": "55, Nagpur, India - 440014",
		"QuoteRef": "12345",
		"RecordType": "",
		"SubsID": "",
		"TotalPrice": 200.00,
		"UserID": 15
	
	}
}




Response:-
{
    "IOSCntUpdateQuoteResult": {
        "BaseDBName": null,
        "BaseSubscriptionID": null,
        "BaseUserID": 0,
        "CustomEditId": 0,
        "FieldName": null,
        "Ip": null,
        "SubscriptionType": null,
        "Code": 100,
        "CustomColumns": null,
        "Data": null,
        "IsPending": 0,
        "Message": "Quote updated successfully.",
        "ScalerData": null
    }
}