BUD-2080

API NAME:- Get Quote Details

URL:-http://localhost:49419/services/QuoteHome/QuoteService.svc/IOSGetQuoteDynamicFormDetails

Request:-
{
	
	"baseEntity" : {
      "BaseDBName" : "BuddyCRMDB_50",
      "BaseSubscriptionID" : "Buddy_50",
      "CustomEditId" : 0,
      "BaseUserID" : 4    
  }
  }



Response:-
{
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "SubscriptionType": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": [
        {
            "FieldDataType": "select",
            "FieldName": "Record Name",
            "FieldNameMaster": "Record Name",
            "ID": 1449,
            "IsExisting": true,
            "IsMandatory": true,
            "IsVisible": true,
            "Priority": "1",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "TextArea",
            "FieldName": "Record Address",
            "FieldNameMaster": "Record Address",
            "ID": 1447,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "2",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "Text",
            "FieldName": "Quote Name",
            "FieldNameMaster": "Quote Name",
            "ID": 1448,
            "IsExisting": true,
            "IsMandatory": true,
            "IsVisible": true,
            "Priority": "3",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "select",
            "FieldName": "Opportunity Link",
            "FieldNameMaster": "Opportunity Link",
            "ID": 1450,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "4",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "Text",
            "FieldName": "Quote Number",
            "FieldNameMaster": "Quote Number",
            "ID": 1451,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": false,
            "Priority": "5",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "Text",
            "FieldName": "Quote Reference",
            "FieldNameMaster": "Quote Reference",
            "ID": 1452,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "6",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "Date",
            "FieldName": "Quote Date",
            "FieldNameMaster": "Quote Date",
            "ID": 1453,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "7",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "image",
            "FieldName": "Quote Image",
            "FieldNameMaster": "QuoteImage",
            "ID": 1456,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "8",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "Text",
            "FieldName": "Salesperson",
            "FieldNameMaster": "Salesperson",
            "ID": 1454,
            "IsExisting": true,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "9",
            "Value": null,
            "dropDownValues": null
        },
        {
            "FieldDataType": "Text",
            "FieldName": "dasdas",
            "FieldNameMaster": "dasdas",
            "ID": 1465,
            "IsExisting": false,
            "IsMandatory": false,
            "IsVisible": true,
            "Priority": "10",
            "Value": null,
            "dropDownValues": null
        }
    ],
    "IsPending": 0,
    "Message": "SUCCESS",
    "ScalerData": null
}