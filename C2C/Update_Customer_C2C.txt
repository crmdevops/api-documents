URL:-http://localhost:49419/Services/Common/CustomerService.svc/IOSCntUpdateCustomer

API NAME:- Update Customer C2C


Request:-
{
	"proEntity": [{
		"ID": "4",
		"FieldName": "CompanyName",
		"Value": "AccountFidler",
		"FieldNameMaster": "CompanyName",
		"IsExisting": 1
	}, {
		"ID": "7",
		"FieldName": "CustomerCode",
		"Value": "Fiddler",
		"FieldNameMaster": "CustomerCode",
		"IsExisting": 1

	}, {
		"ID": "8",
		"FieldName": "Website",
		"Value": "http://www.fidler.com",
		"FieldNameMaster": "Website",
		"IsExisting": 1
	}, {
		"ID": "9",
		"FieldName": "Twitter",
		"Value": "http://www.fidlerTwitter.com",
		"FieldNameMaster": "Twitter",
		"IsExisting": 1
	},
 {
		"ID": "1249",
		"FieldName": "text area pp",
		"Value": "text area pp",
		"FieldNameMaster": "text area pp",
		"IsExisting": 0
	},
 {
		"ID": "6",
		"FieldName": "OwnerID",
		"Value": "1",
		"FieldNameMaster": "OwnerID",
		"IsExisting": 1
	},
 {
		"ID": "5",
		"FieldName": "ParentAccID",
		"Value": "4",
		"FieldNameMaster": "ParentAccID",
		"IsExisting": 1
	},
	{
		"ID": "17",
		"FieldName": "JobTitle",
		"Value": "testssasasa",
		"FieldNameMaster": "JobTitle",
		"IsExisting": 1
	}
],
	"baseEntity": {
		"BaseUserID": 1,
		"BaseSubscriptionID": "Buddy_50",
		"BaseDBName": "BuddyCRMDB_50",
		"CustomEditId": 0
	},
	"accountId":20411,
	"completeAddress": "New Delhi",
	"IsMasterFields": 1
}


Response:-
{
  "IOSCntUpdateCustomerResult": {
    "BaseDBName": null,
    "BaseSubscriptionID": null,
    "BaseUserID": 0,
    "CustomEditId": 0,
    "FieldName": null,
    "Ip": null,
    "SubscriptionType": null,
    "Code": 100,
    "CustomColumns": null,
    "Data": null,
    "Message": "Record Updated Successfully",
    "ScalerData": {
      "ID": 0,
      "Message": null,
      "isPending": 0
    },
    "isPending": 0
  }
}
